// XULRunner settings
pref("toolkit.defaultChromeURI", "chrome://emusic/content/hidden.xul");
//pref("SSStoolkit.defaultChromeURI", "chrome://emusic/content/startup.xul");
pref("browser.chromeURL", "chrome://emusic/content/hidden.xul"); //"chrome://emusic/content/startup.xul");
//pref("toolkit.singletonWindowType", "emusic:hidden");

pref("general.useragent.vendor", "eMusic DLM");
pref("general.useragent.vendorSub", "4.0/1.0.0.2");
pref("general.useragent.vendorComment", "http://www.emusic.com/");
pref("general.useragent.extra.notfox", "(like Firefox/2.0.0.8)");

pref("eMusic.majorRevision", "");

// Download Manager Specific Settings
pref("eMusic.download.dir", "");
pref("eMusic.download.downloadDir", "");
pref("eMusic.download.folderList", 2);
pref("eMusic.download.useDownloadDir", true);

pref("browser.download.show_plugins_in_list", true);
pref("browser.download.hide_plugins_without_extensions", false);

pref("plugin.disable_full_page_plugin_for_types", "application/vnd.emusic-emusic_list,audio/x-mpegurl");

pref("eMusic.defaultHomePage", "http://www.emusic.com/?show");
pref("eMusic.defaultServer", "http://www.emusic.com/");
pref("eMusic.developerHomepage", "http://developer.emusic.com/?ver=%VERSION%&build=%APPBUILDID%&os=%OS%&target=%TARGET%");
pref("eMusic.developerFeedback", "http://developer.emusic.com/dlm/feedback?ver=%VERSION%&build=%APPBUILDID%&os=%OS%&target=%TARGET%");

pref("eMusic.defaultSearchURL", "search.html?mode=x&QT=");

pref("eMusic.defaultFREF", "702514");

pref("eMusic.treeViewPivot", "genre");

pref("eMusic.defaultMediaPlayerObj", "DefaultPlayer");
pref("eMusic.defaultMediaSync", "");
pref("eMusic.defaultMediaPlayer", "DefaultPlayer");
pref("eMusic.defaultMediaPlayer.Linux", "/usr/bin/totem");

pref("eMusic.FileNamingTemplate", "artist_number_track"); //album - artist - number - track
pref("eMusic.FileNamingSeparator", "_");
pref("eMusic.FileNamingOrder", "fnArtist|fnAlbum|fnTrackNum|fnTrackTitle|");

pref("eMusic.alerts.slideIncrementTime", 50);
pref("eMusic.alerts.totalOpenTime", 3000);

pref("eMusic.CreateArtistDirectory", true);
pref("eMusic.CreateAlbumDirectory", true);
pref("eMusic.AutoClearCancelled", false);
pref("eMusic.QuitWhenDone", false);

pref("eMusic.template.artist", true);
pref("eMusic.template.album", false);
pref("eMusic.template.number", true);
pref("eMusic.template.track", true);

pref("eMusic.templateFile", "chrome://emusic/locale/info.html");
pref("eMusic.templateFileAudioBooks", "chrome://emusic/locale/info_audiobooks.html");

pref("eMusic.preferences.general.selectedTabIndex", 0);

pref("eMusic.saveAlbumArt", true);

pref("eMusic.firstrun", true);
pref("eMusic.firstrun.homepage", "http://www.emusic.com/promo/remote_demo/index.html");
pref("eMusic.debugMode", false);

pref("eMusic.currentDeck", 0);
pref("eMusic.downloadDeck", 0);
pref("eMusic.lastUrl", "http://www.emusic.com/?show");
pref("eMusic.cmdLineValue", "");

pref("eMusic.domain_whitelist", "emusic.com$|optimost.com$|17dots.com$");

// Basic Settings
pref("browser.chrome.site_icons", true);
pref("browser.chrome.favicons", true);
pref("browser.tabs.showSingleWindowModePrefs", true);
pref("browser.tabs.selectOwnerOnClose", true);
pref("browser.tabs.loadInBackground", true);
pref("browser.tabs.autoHide", true);

pref("browser.download.useDownloadDir", false);
pref("browser.download.manager.showAlertOnComplete", false);
pref("browser.download.manager.showWhenStarting", false);
pref("browser.download.manager.useWindow", false);
pref("browser.download.manager.closeWhenDone", false);
pref("browser.download.manager.showAlertInterval", 2000);
pref("browser.download.manager.retention", 2);
pref("browser.download.manager.openDelay", 0);

pref("browser.download.manager.addToRecentDocs", false);

pref("alerts.slideIncrement", 1);
pref("alerts.slideIncrementTime", 10);
pref("alerts.totalOpenTime", 4000);
pref("alerts.height", 50);

pref("accessibility.typeaheadfind", false);
pref("accessibility.typeaheadfind.flashBar", 0);
pref("accessibility.typeaheadfind.timeout", 3000);
pref("accessibility.typeaheadfind.linksonly", false);

pref("ui.submenuDelay", 0);
pref("general.autoScroll", false);
pref("general.smoothScroll", false);

pref("signon.rememberSignons", true);
pref("signon.expireMasterPassword", false);
pref("signon.SignonFileName", "signons.txt");
pref("browser.formfill.enable", true);

pref("layout.spellcheckDefault", 1);

//pref("middlemouse.openNewWindow", false);
pref("dom.disable_window_move_resize", true);
pref("dom.max_chrome_script_run_time", 60);
pref("dom.max_script_run_time", 30);

pref('extensions.minimizetotray.always', true);
pref('extensions.minimizetotray.minimize-on-close', false);
pref('extensions.minimizetotray.two-click-restore', false);

//pref("browser.chrome.toolbar_style", 1);

// App update prefs
pref("app.update.enabled", true);
pref("app.update.auto", false);
pref("app.update.interval", 86400);
pref("app.update.incompatible.mode", 0);
pref("app.update.mode", 1);
pref("app.update.nagTimer.download", 86400);
pref("app.update.nagTimer.restart", 1800);
pref("app.update.silent", false);
pref("app.update.timer", 600000);
pref("app.update.url", "http://developer.emusic.com/dlm/update/%VERSION%/%BUILD_ID%/%BUILD_TARGET%/");

pref("app.update.url.details", "http://developer.emusic.com/");
pref("app.update.url.manual", "http://developer.emusic.com/");

// Default Extensions/Themes Prefs
pref("extensions.update.url", "chrome://emusic/locale/emusic.properties");
pref("extensions.getMoreExtensionsURL", "chrome://emusic/locale/emusic.properties");
pref("extensions.getMoreThemesURL", "chrome://emusic/locale/emusic.properties");
pref("xpinstall.whitelist.add", "www.emusic.com");
pref("xpinstall.whitelist.add.103", "www.emusic.com");

pref("extensions.blocklist.enabled", false);

pref("extensions.update.enabled", true);
pref("extensions.update.interval", 86400);
pref("extensions.dss.enabled", false);
pref("extensions.dss.switchPending", false);
pref("extensions.ignoreMTimeChanges", false);
pref("extensions.logging.enabled", false);
pref("general.skins.selectedSkin", "classic/1.0");

// Define warnings and other protocol prefs
pref("security.warn_entering_secure", false);
pref("security.warn_leaving_secure", false);
pref("security.warn_submit_insecure", false);

pref("security.enable_java", false);

pref("network.protocol-handler.warn-external.http", false);
pref("network.protocol-handler.warn-external.https", false);
pref("network.protocol-handler.warn-external.ftp", false);
pref("network.protocol-handler.warn-external.mailto", false);
pref("network.protocol-handler.warn-external.emusic", false);
pref("network.protocol-handler.warn-external.chrome", false);

pref("network.protocol-handler.external.mailto", true);
pref("network.protocol-handler.external.news", true);
pref("network.protocol-handler.external.snews", true);
pref("network.protocol-handler.external.nntp", true);

// Small UI optimization tweaks
pref("config.trim_on_minimize", false);
pref("nglayout.initialpaint.delay", 0);
pref("ui.submenuDelay", 0);
pref("browser.display.show_image_placeholders", true);

// Enable pipelining for improved performance:
// pref("network.http.pipelining", true);
// pref("network.http.proxy.pipelining", true);
// pref("network.http.pipelining.firstrequest", true);
// pref("network.http.pipelining.maxrequests", 4 );

// debug preferences
pref("javascript.options.showInConsole", true);
pref("javascript.options.strict", true);
pref("browser.xul.error_pages.enabled", true);
// debug: end

/*
pref("app.update.log.Checker", true);
pref("app.update.log.Downloader", true);
pref("app.update.log.General", true);
pref("app.update.log.UI:CheckingPage", true);
pref("app.update.log.UI:DownloadingPage", true);
pref("app.update.log.UI:LicensePage", true);
pref("app.update.log.UpdateManager", true);
pref("app.update.log.UpdateService", true);
*/